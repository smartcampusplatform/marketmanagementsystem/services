const { Router: router } = require('express');

const app = router();

app.get('/add', function(req, res, next) {
    res.send("Add GET");
})

app.post('/add', function(req, res, next) {
    res.send("Add POST");
})

module.exports = app;